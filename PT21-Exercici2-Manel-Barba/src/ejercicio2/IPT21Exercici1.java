package ejercicio2;

import java.sql.SQLException;

public interface IPT21Exercici1 {
	public void reset() throws SQLException;

	public void mostrarClients() throws SQLException;

	public void mostrarFactures() throws SQLException;

	public void inserirClientTeclat() throws SQLException;

	public void inserirFacturaTeclat() throws SQLException;

	void inserirClient(Client client) throws SQLException;

	void inserirFactura(Factura factura) throws SQLException;

	public void consultaNomsFactures() throws SQLException;
	

}

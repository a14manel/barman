package ejercicio2;

public class Factura {

	private int idFactura;
	private String nom;
	
	public Factura(int id, String nom) {
		
		this.idFactura = id;
		this.nom = nom;
	}
	public int getId() {
		return idFactura;
	}
	public void setId(int id) {
		this.idFactura = id;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}

}

package ejercicio2;

import java.util.ArrayList;

public class Client {

	private int id;
	private String nom;
	private ArrayList<Factura> facturas;
	
	public Client(int id, String nom,ArrayList<Factura> facturas) {
		
		this.id = id;
		this.nom = nom;
		this.facturas = facturas;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public ArrayList<Factura> getFacturas() {
		return facturas;
	}

	public void setFacturas(ArrayList<Factura> facturas) {
		this.facturas = facturas;
	}
	
}

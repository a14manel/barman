package ejercicio2;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Scanner;

public class Principal {

	static final String URL = "jdbc:hsqldb:file:BDPruebas";
	static final String USER = "march";
	static final String PASS = "iesam38";

	public static void main(String[] args) throws SQLException {

		Principal principal = new Principal();

		Scanner input = new Scanner(System.in);

		int opcion = 0;
		System.out.println("Escoge una opcion: \n1)Reset.\n2)ImprimeixClient.\n3)MostrarFactures." + "\n4)Salir.");

		opcion = input.nextInt();

		switch (opcion) {
		case 1:

			principal.reset();

			break;
		case 2:

			principal.ImprimeixClient();

			break;
		case 3:

			System.out.println("introduce el cliente a buscar");

			String nombre = input.next();

			principal.buscaClientPerNom(nombre);

			break;
		case 4:

			System.out.println("Gracais por usar el programa");

			break;

		default:

			System.out.println("opcion incorrecta");

			break;
		}
		input.close();
	}

	public void reset() throws SQLException {

		Connection con = DriverManager.getConnection(URL, USER, PASS);
		Statement state = con.createStatement();

		String drop = " drop table clientes if exists ;";
		String drop2 = " drop table facturas if exists ;";

		state.executeUpdate(drop);
		state.executeUpdate(drop2);

		String crear = "create table clientes (id int PRIMARY KEY,nom varchar(35)); ";
		String crear2 = "create table facturas (id int,nom varchar(35),numCliente int); ";

		state.executeUpdate(crear);
		state.executeUpdate(crear2);

		String datos = "insert into clientes values (1,'albondiga');";
		String datos2 = "insert into clientes values (2,'shaun');";
		String datos3 = "insert into facturas values (1,'estimulantes',1);";
		String datos4 = "insert into facturas values (2,'synt',2);";

		state.executeUpdate(datos);
		state.executeUpdate(datos2);
		state.executeUpdate(datos3);
		state.executeUpdate(datos4);

		state.close();
		con.close();
	}

	public void ImprimeixClient() throws SQLException {

		Connection con = DriverManager.getConnection(URL, USER, PASS);

		Statement state = con.createStatement();

		String selectClientes = "select * from clientes";

		ResultSet resultado = state.executeQuery(selectClientes);

		while (resultado.next()) {

			int id = resultado.getInt(1);
			String nombre = resultado.getString(2);
			System.out.println("|" + id + " | " + nombre + "|");
		}

		state.close();
		con.close();
	}

	public void guardaOActualitzaClient(Client client) throws SQLException {

		int id = client.getId();
		String nombre = client.getNom();
		ArrayList<Factura> facturas = client.getFacturas();

		Connection con = DriverManager.getConnection(URL, USER, PASS);
		Statement state = con.createStatement();

		state.close();
		con.close();
	}

	public Client buscaClientPerNom(String nombre) throws SQLException {

		Connection con = DriverManager.getConnection(URL, USER, PASS);
		Statement state = con.createStatement();

		String select = "SELECT id FROM clientes where nom like '" + nombre + "';";

		ResultSet resultado = state.executeQuery(select);

		int idCliente = 0;

		while (resultado.next()) {
			idCliente = resultado.getInt(1);

		}

		String selectFacturas = "SELECT * FROM facturas where numCliente = " + idCliente + ";";

		ResultSet resultadoFacturas = state.executeQuery(selectFacturas);

		ArrayList<Factura> facturas = new ArrayList<Factura>();

		while (resultadoFacturas.next()) {
			int idFactura = resultado.getInt(1);
			System.out.println(idFactura);
			String nombreFact = resultado.getString(2);
			System.out.println(nombreFact);
			Factura fac = new Factura(idFactura, nombreFact);
			facturas.add(fac);

		}

		Client cliente = new Client(idCliente, nombre, facturas);

		state.close();
		con.close();

		return cliente;

	}

}
